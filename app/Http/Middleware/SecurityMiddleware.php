<?php

namespace App\Http\Middleware;

use Closure;

class SecurityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->get('access_key') != getenv('ACCESS_KEY') &&
          $request->get('access_secret') != getenv('ACCESS_SECRET')) {

//            abort(401, "Unauthorized");
            return response('Unauthorized', 401);
        }

        return $next($request);
    }
}
