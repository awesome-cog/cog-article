<?php
namespace App\Http\Controllers;

use App\Model\Article;
use Illuminate\Http\Request;
use Michelf\Markdown;

class ArticleController extends Controller
{
    public function storage(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);
        $content = $request->get("content");
        $content = Markdown::defaultTransform($content);

        $article = new Article($request->all());
        $article->compiled = $content;
        $article->save();

        return $article;
    }

    public function get(Request $request, $id)
    {
        return Article::find($id)->pagination(10);
    }

    public function index()
    {
        return Article::all(['id', 'title', 'created_at', 'updated_at']);
    }
}